# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Attach2',
    'version': '1.0',
    'category': 'Reports',
    'sequence': 15,
    'summary': 'Qweb Reports for Quotation,Sale Orders, Purchase, Invoice',
    'description': """
    Qweb Reports for Quotation,Sale Orders, Purchase, Invoice
    """,
    'website': 'http://www.pragtech.co.in/',
    'depends': ['sale','purchase','account'],
    'data': [
        'views/template.xml',
        'data/paperformat.xml',
#         'reports/stock_report.xml',
#         'reports/packing_slip_report.xml',
        'reports/sale_report_templates.xml',
        'reports/sale_report.xml',
        'reports/invoice_report.xml',
        'reports/invoice_report_template.xml',
        'reports/purchase_report.xml',
        'reports/purchase_order_template.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
