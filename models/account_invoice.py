# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class AccountAccount(models.Model):
    _inherit = 'account.invoice'

    @api.one
    def get_sale_order(self):
        sale_order_object = self.env['sale.order'].search([('name','=',self.origin)])
        return sale_order_object
        if sale_order_object:
            sale_order_object = sale_order_object[0]
            return sale_order_object